import json
from tkinter import *
from tkinter import ttk
import datetime
from os.path import dirname, join

current_dir = dirname(__file__)
data_file = join(current_dir, "./data.json")
studentList = []
sorting = ''

def update_students():
    with open(data_file, 'w') as file:        
        for student in studentList:
            json_data = json.dumps(student)
            file.write(json_data + '\n')

def start_timer():
    current_time = datetime.datetime.now().time().strftime('%H:%M:%S')
    for student in studentList:           
            student['start_time'] = current_time
    update_students()
    get_studentList()

def lap_timer():
    current_time = datetime.datetime.now().time().strftime('%H:%M:%S')
    selected = 0
    studentNum = ''
    # selected = timer_student_numberE.get().replace(" ", "")
    # r = selected.replace(" ", "")
    # x = selected.split(',')
    # print(selected)
    # print(x)
    # for i in x:
        # k = int(i) -1
        # print(i)
        # print(k)
        # print(x[k])
    try:
        selected = int(tv.focus()) - 1
    except:
        print()
    finally:
        studentList[selected]['lap_time'] = current_time
        update_students()
        get_studentList()
    try:
        studentNum = timer_student_numberE.get()
    except:
        print()
    finally:
        if timer_student_numberE.get() != '':
            # studentNum = selected
        
            for student in studentList:
                if student['number'] == studentNum:
                    student['lap_time'] = current_time
        update_students()
        get_studentList()
    # timer_student_numberE.delete(0, END)
    # if int(tv.focus()) >= 1:
    #     selected = int(tv.focus()) - 1
    #     studentList[selected]['lap_time'] = current_time
    #     for student in studentList:
    #             if student['number'] == selected:
    #                 student['lap_time'] = current_time
    

def edit_data(field, studentinfo = None, student_ = None):
    if field == 'start_time':
        for student in studentList:
            current_time = datetime.datetime.now().time().strftime('%H:%M:%S')
            student[field] = current_time
    if field == "lap_time":
        for student in studentList:
            if student[student_] == studentinfo:
                current_time = datetime.datetime.now().time().strftime('%H:%M:%S')
                student[field] = current_time
    update_students()
    get_studentList()

def edit_student():
    input_frame.grid_remove()
    timer_menuF.grid_remove()
    edit_studentF.grid()
    selected = int(tv.focus()) - 1
    update_studentNumberE.insert(0, studentList[selected]['number'])
    update_studentNameE.insert(0, studentList[selected]['name'])
    update_studentGenderE.insert(0, studentList[selected]['gender'])
    update_studentStarttimeE.insert(0, studentList[selected]['start_time'])
    update_studentLaptimeE.insert(0, studentList[selected]['lap_time'])
    # print(studentList[selected])

def update_student():
    selected = int(tv.focus()) - 1
    studentList[selected] = {'number': update_studentNumberE.get(), 'name': update_studentNameE.get(), 'gender': update_studentGenderE.get(), 'start_time': update_studentStarttimeE.get(), 'lap_time': update_studentLaptimeE.get()}
    update_students()
    get_studentList()

def add_student_menu():
    edit_studentF.grid_remove()
    timer_menuF.grid_remove()
    input_frame.grid()

def add_student():
    num = student_number.get()
    name = student_name.get()
    gender = student_gender.get()
    student = {'number': num, 'name': name, 'gender': gender, 'start_time': '', 'lap_time': ''}
    json_data = json.dumps(student)
    with open(data_file, 'a') as file:
        file.write(json_data + '\n')
    get_studentList()


def menu():
    print("Select option:")
    print("1 Add student")
    print("2 Start timer")
    print("3 Lap time")
    print("4 Edit student")
    print("5 Print database")
    data = input("Input option:")
    print("selected: " + str(data))

    if data == str(1):
        num = input("Student number: ")
        name = input("Student name: ")
        gender = input("Students gender: ")
        student = {'number': num, 'name': name, 'gender': gender, 'start_time': '', 'lap_time': ''}
        json_data = json.dumps(student)
        with open(data_file, 'a') as file:
            file.write(json_data + '\n')
            # json.dump(student, file)
            # file.write('\n')

    if data == str(2):
        edit_data('start_time')
        # for student in studentList:
        #     current_time = datetime.datetime.now().time().strftime('%H:%M:%S')
        #     student['start_time'] = current_time
        # update_students()

    if data == str(3):
        number = input("Student number:")
        edit_data('lap_time', number, 'number')
        # for student in studentList:
        #     if student['number'] == number:
        #         current_time = datetime.datetime.now().time().strftime('%H:%M:%S')
        #         student['lap_time'] = current_time
        # update_students()

    if data == str(4):
        edit = input("Edit student by number (1) or name (2) ?")
        
        if edit == str(1):
            number = input("Enter student number: ")
            
            for student in studentList:
                if student['number'] == number:
                    print('there')
                    print(student)
                    time = input("Start time:")
                    student['start_time'] = time
                    print(student)
                    break
                else:
                    print('not there')
        update_students()

    if data == str(5):
        for student in studentList:
            print(student)

def sort(e):
    return int(e[sorting])

def sortCustom(e):
    if variable.get() == 'number':
        return int(e[variable.get()])
    else:
        return e[variable.get()]

def get_studentList():
    studentList.clear()
    with open(data_file, 'r') as file:
        for jsonObj in file:
            db = json.loads(jsonObj)
            studentList.append(db)
    print_studentList('number')
        # menu()

def print_studentList(e):
    i = 0
    for k in tv.get_children():
        tv.delete(k)
    studentList.sort(key=sortCustom)
    for student in studentList:
        i += 1
        tv.insert(parent='', index=i, iid=i, text='', values=(student['number'],student['name'],student['gender'],student['start_time'],student['lap_time']))


def print_value(e):
    # value = student_number.get()
    print("Value:", e)

def test_command():
    input_frame.grid()
    edit_studentF.grid_remove()

def timer_menu():
    input_frame.grid_remove()
    edit_studentF.grid_remove()
    timer_menuF.grid()


ws = Tk()
ws.title('Data Tracking')
ws.geometry('800x600')
# ws['bg']='#fb0'

choices = ('number', 'name', 'gender', 'start_time', 'lap_time')
variable = StringVar()
variable.set(choices[0])

menu_frame = LabelFrame(ws, bd=0)
menu_frame.grid(row=0, column=4, columnspan=3, sticky='W', \
                padx=5, pady=5, ipadx=5, ipady=5)

sort_option = OptionMenu(menu_frame, variable, *choices, command=print_studentList)
sort_option.grid(row=0, column=0)

add_student_button = Button(menu_frame, text="Add Student", command=add_student_menu)
add_student_button.grid(column=0, row=2)

edit_studentB = Button(menu_frame, text="Edit Student", command=edit_student)
edit_studentB.grid(column=0, row=3)

timers_button = Button(menu_frame, text="Timer Menu", command=timer_menu)
timers_button.grid(column=1, row=0)
# sort_button = Button(menu_frame, text="Sort", command=sortCustom)
# sort_button.grid(column=0 ,row=1)

timer_menuF = LabelFrame(ws, bd=0)
timer_menuF.grid(row=0, columnspan=3, sticky='W', \
                 padx=5, pady=5, ipadx=5, ipady=5)
timer_menuF.grid_remove()

timer_student_numberL = Label(timer_menuF, show=None, text="Student Number")
timer_student_numberL.grid(column=0, row=0)

timer_student_numberE = Entry(timer_menuF, show=None)
timer_student_numberE.grid(column=1, row=0)

lap_timeB = Button(timer_menuF, text="Set Lap Time", command=lap_timer)
lap_timeB.grid(column=2, row=0)

start_timeB = Button(timer_menuF, text="Set Start Time", command=start_timer)
start_timeB.grid(column=2, row=1)


input_frame = LabelFrame(ws, bd=0)
input_frame.grid(row=0, columnspan=3, sticky='W', \
                 padx=5, pady=5, ipadx=5, ipady=5)
input_frame.grid_remove()

student_number_label = Label(input_frame, show=None, text="Student Number")
student_number_label.grid(column=0, row=0)
student_number = Entry(input_frame, show=None)
student_number.grid(column=1, row=0)
# student_number.pack(padx=5, pady=15, side="top")

student_name_label = Label(input_frame, show=None, text="Student Name")
student_name_label.grid(column=0, row=1)
student_name = Entry(input_frame, show=None)
student_name.grid(column=1, row=1)
# student_name.pack(side="top")

student_name_label = Label(input_frame, show=None, text="Student Gender")
student_name_label.grid(column=0, row=2)
student_gender = Entry(input_frame, show=None)
student_gender.grid(column=1, row=2)

add_studentB = Button(input_frame, text="Add Student", command=add_student)
# add_student = ttk.Button(ws, text="Add Student", command=add_student(student_number.get(),student_name.get(),student_gender.get()))
add_studentB.grid(column=1, row=3)


edit_studentF = LabelFrame(ws, bd=0)
edit_studentF.grid(row=0, columnspan=2, sticky='W', \
                 padx=5, pady=5, ipadx=5, ipady=5)
edit_studentF.grid_remove()

update_studentNumberL = Label(edit_studentF, show=None, text="Student Number")
update_studentNumberL.grid(column=0, row=0)
update_studentNumberE = Entry(edit_studentF, show=None)
update_studentNumberE.grid(column=1, row=0)

update_studentNameL = Label(edit_studentF, show=None, text="Student Name")
update_studentNameL.grid(column=0, row=1)
update_studentNameE = Entry(edit_studentF, show=None)
update_studentNameE.grid(column=1, row=1)

update_studentGenderL = Label(edit_studentF, show=None, text="Gender")
update_studentGenderL.grid(column=0, row=2)
update_studentGenderE = Entry(edit_studentF, show=None)
update_studentGenderE.grid(column=1, row=2)

update_studentStarttimeL = Label(edit_studentF, show=None, text="Start Time")
update_studentStarttimeL.grid(column=0, row=3)
update_studentStarttimeE = Entry(edit_studentF, show=None)
update_studentStarttimeE.grid(column=1, row=3)

update_studentLaptimeL = Label(edit_studentF, show=None, text="Lap Time")
update_studentLaptimeL.grid(column=0, row=4)
update_studentLaptimeE = Entry(edit_studentF, show=None)
update_studentLaptimeE.grid(column=1, row=4)

update_studentB = Button(edit_studentF, text="Update Student", command=update_student)
update_studentB.grid(column=1, row=5)

tv_frame = LabelFrame(ws, bd=0)
tv_frame.grid(row=1, columnspan=2, sticky='W', \
                 padx=5, pady=5, ipadx=5, ipady=5)

tv = ttk.Treeview(tv_frame)
tv['columns']=('Number', 'Name', 'Gender', 'Start Time', 'Lap Time')
tv.column('#0', width=0, stretch=NO)
tv.column('Number', anchor=CENTER, width=80)
tv.column('Name', anchor=CENTER, width=80)
tv.column('Gender', anchor=CENTER, width=80)
tv.column('Start Time', anchor=CENTER, width=80)
tv.column('Lap Time', anchor=CENTER, width=80)

tv.heading('#0', text='', anchor=CENTER)
tv.heading('Number', text='Number', anchor=CENTER)
tv.heading('Name', text='Name', anchor=CENTER)
tv.heading('Gender', text='Gender', anchor=CENTER)
tv.heading('Start Time', text='Start Time', anchor=CENTER)
tv.heading('Lap Time', text='Lap Time', anchor=CENTER)
# tv.insert(parent='', index=0, iid=0, text='', values=('1','Marcus','Male','20:49:30','20:55:34'))
# tv.insert(parent='', index=1, iid=1, text='', values=('5','Loki','Male','20:45:30','20:55:54'))
tv.grid(column=0, row=0, columnspan=2)
# tv.pack(side="bottom")

get_studentList()
ws.mainloop()